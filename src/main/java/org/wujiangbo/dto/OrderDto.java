package org.wujiangbo.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 订单信息
 */
@ToString
@Data
public class OrderDto implements Serializable {

    private String id;//订单编号
    private Long userId;//下单的用户ID
    private Double orderMoney;//订单金额
    private String orderStatus;//订单状态（0：待支付；1：已支付；2：未支付(超时)）
}
