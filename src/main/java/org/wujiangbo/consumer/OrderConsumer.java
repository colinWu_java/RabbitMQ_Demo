package org.wujiangbo.consumer;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import java.io.IOException;

/**
 * @desc 订单消费者：专门处理订单消息
 * @author wujiangbo
 * @date 2022-04-02 10:16
 */
@Component
public class OrderConsumer {

    /**
     * 监听订单队列
     */
    @RabbitListener(queues = {"normalQueue"})
    public void handleOrderQueue(Message message, Channel channel) throws IOException {
        System.out.println("订单消费者类，拿到消息：" + new String(message.getBody()));
        try {
            //开始处理消息
            //模拟处理消息花费时间
            Thread.sleep(2000);
            //int i = 1/0;//模拟：处理消息时发生了某种异常

            //消息处理完毕，一定要记得手动ACK
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (Exception e){
            /**
             * 发生异常，进行NACK操作
             * requeue参数：
             *      true-消息回队列重新拍断;
             *      false-没有配置死信交换机的话：消息会丢失
             *            配置了死信交换机的话：消息会变成死信消息
             */
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
        }
    }

    /**
     * 监听死信队列
     */
    @RabbitListener(queues = {"deadQueue"})
    public void handleDeadQueue(Message message, Channel channel) throws IOException {
        System.out.println("拿到死信消息：" + new String(message.getBody()));
        System.out.println("properties===" + message.getMessageProperties());
        /**
         * 这里取到消息后，根据实际业务规则，进行处理即可
         */
        try {
            //模拟处理消息花费时间
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //最后手动ACK
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }
}